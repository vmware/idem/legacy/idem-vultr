ensure_user_absent:
  vultr.user.absent:
    - name: idemcloud testuser

ensure_startup_absent:
  vultr.script.absent:
    - name: startup.sh

ensure_ssh_key_absent:
  vultr.ssh_key.absent:
    - name: idem-vultr-rsa

ensure_instance_absent:
  vultr.server.vm.absent:
    - name: idem_test_vm

ensure_baremetal_absent:
  vultr.baremetal.server.absent:
    # This state is using a different `acct_profile` from the rest of the states
    - acct_profile: baremetal_profile
    - name: idem_baremetal_server

ensure_loadbalancer_absent:
  vultr.loadbalancer.subscription.absent:
    - name: idem_loadbalancer

ensure_object_cluster_absent:
  vultr.object_storage.subscription.absent:
    - name: idem_loadbalancer

# Cleaning up a firewall group also destroys the rules within
ensure_firewall_group_absent:
  vultr.firewall.group.absent:
    - name: idem_firewall_group

